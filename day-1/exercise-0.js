export const my_sum = (a, b) => {
  // verifie qu'on a bien 2 paramètres
  if (!a || !b) {
    return 0;
  }
  // verifie que a et b sont bien des nombres
  if (typeof a === 'number' && typeof b === 'number') {
    // retourne la somme
    return a + b;
  }
  return 0;
};
export const my_size_alpha = (str) => {
  let i = 0;
  // verifie que le paramètres est une string
  if(typeof(str) !== 'string') {
    return 0;
  }
  while(str[i]){
    i++;
  }
  return i;
};

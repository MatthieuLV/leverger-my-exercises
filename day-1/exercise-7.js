export const my_is_posi_neg = (nbr) => {
  if (nbr > 0 || nbr === undefined || nbr === null) {
    return "POSITIF"; // Retourne POSITIF si le paramètres est supérieur à 0, null ou undefined
  } else if (nbr === 0) {
    return "NEUTRAL"; // Retourne NEUTRAL si le paramètres est égale à 0
  } else {
    return "NEGATIF"; // Retourne NEUTRAL sinon
  }
};

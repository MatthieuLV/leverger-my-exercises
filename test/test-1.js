import assert from "assert";
import { my_display_alpha } from "../day-1/exercise-1.js";

describe("Exercise 1", function () {

  it("should return a string from a to z", function () {
    var test = my_display_alpha();
    assert.equal(test, "abcdefghijklmnopqrstuvwxyz");
  });
});

import { my_alpha_number } from "../day-1/exercise-3.js";
import { expect } from 'chai';

describe("Exercise 3", function () {

  it("should return a string", function () {
    var test = my_alpha_number(10);
    expect(test).to.be.a('string');
  });

  it("should return a string", function () {
    var test = my_alpha_number(-10);
    expect(test).to.be.a('string');
  });

  it("should return a string", function () {
    var test = my_alpha_number('10');
    expect(test).to.be.a('string');
  });

  it("should return a string", function () {
    var test = my_alpha_number();
    expect(test).to.be.a('string');
  });
});

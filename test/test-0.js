import assert from "assert";
import { my_sum } from "../day-1/exercise-0.js";

describe("Exercise 0", function () {

  it("should return 3", function () {
    var test = my_sum(1,2);
    assert.equal(test, 3);
  });

  it("should return -3", function () {
    var test = my_sum(-5,2);
    assert.equal(test, -3);
  });

//  it("should return 2", function () {
//    var test = my_sum(0,2);
//    assert.equal(test, 2);
//  });

//  it("should return -10", function () {
//    var test = my_sum(-5,-5);
//    assert.equal(test, -5);
//  });

  it("should return 0", function () {
    var test = my_sum(1,'a')
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_sum('a',1)
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_sum('a','b')
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_sum(1)
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_sum('a')
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_sum()
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_sum(['a','b','c'])
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_sum(['a','b','c'], 1)
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_sum(['a','b','c'], 'a')
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_sum(['a','b','c'], ['a','b','c'])
    assert.equal(test, 0);
  });
});

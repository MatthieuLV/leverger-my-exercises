import assert from "assert";
import { my_size_alpha } from "../day-1/exercise-4.js";

describe("Exercise 4", function () {

  it("should return 8", function () {
    var test = my_size_alpha("matthieu");
    assert.equal(test, 8);
  });

  it("should return 0", function () {
    var test = my_size_alpha(123);
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_size_alpha();
    assert.equal(test, 0);
  });

  it("should return 0", function () {
    var test = my_size_alpha(['a','b','c']);
    assert.equal(test, 0);
  });
});

import { my_array_alpha } from "../day-1/exercise-5.js";
import * as chai from 'chai';

describe("Exercise 5", function () {

  it("should return an array with 8 items", function () {
    var test = my_array_alpha("matthieu");
    chai.assert.isTrue(Array.isArray(test));
    chai.assert.equal(test.length, 8);
  });
});

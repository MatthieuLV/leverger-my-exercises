import assert from "assert";
import { my_display_alpha_reverse } from "../day-1/exercise-2.js";

describe("Exercise 2", function () {

  it("should return a string from z to a", function () {
    var test = my_display_alpha_reverse();
    assert.equal(test, "zyxwvutsrqponmlkjihgfedcba");
  });
});

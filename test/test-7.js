import { my_is_posi_neg } from "../day-1/exercise-7.js";
import * as chai from 'chai';

describe("Exercise 7", function () {

  it("should return POSITIF", function () {
    var test = my_is_posi_neg(5);
    chai.assert.equal(test, "POSITIF");
  });

  it("should return POSITIF", function () {
    var test = my_is_posi_neg(null);
    chai.assert.equal(test, "POSITIF");
  });

  it("should return POSITIF", function () {
    var test = my_is_posi_neg(undefined);
    chai.assert.equal(test, "POSITIF");
  });

  it("should return NEGATIF", function () {
    var test = my_is_posi_neg(-5);
    chai.assert.equal(test, "NEGATIF");
  });

  it("should return NEGATIF", function () {
    var test = my_is_posi_neg('a');
    chai.assert.equal(test, "NEGATIF");
  });

  it("should return NEUTRAL", function () {
    var test = my_is_posi_neg(0);
    chai.assert.equal(test, "NEUTRAL");
  });
});

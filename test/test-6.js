import { my_length_array } from "../day-1/exercise-6.js";
import * as chai from 'chai';

describe("Exercise 6", function () {

  it("should return 4", function () {
    var array = [1,2,3,4];
    var test = my_length_array(array);
    chai.assert.equal(test, 4);
  });

  it("should return 4", function () {
    var array = ['a','b','c','d'];
    var test = my_length_array(array);
    chai.assert.equal(test, 4);
  });
});

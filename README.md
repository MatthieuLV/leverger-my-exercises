# My Exercises


## Resume

- Exercise 0 : my_sum(a,b) returns the result of a + b
- Exercise 1 : my_display_alpha() return alphabet in lowercase
- Exercise 2 : my_display_alpha_reverse() return reverse alphabet in lowercase
- Exercise 3 : my_alpha_number(nbr) return a number as a string
- Exercise 4 : my_size_alpha(str) count caracters in a string
- Exercise 5 : my_array_alpha(str) return an array of all caracters in a string
- Exercise 6 : my_length_array(arr) count all elements in an array
- Exercise 7 : my_is_posi_neg(nbr) return POSITIF if a number is positive, null or undefined,
return NEUTRAL if it equal to zero and return NEGATIF if it is negative


## Technologies
- Node Js 
- Mocha
- Chai

## Install project

Clone this project by running
```
git clone git@gitlab.com:MatthieuLV/leverger-my-exercises.git
```

Then run 
```
npm install
```

## Run an exercise 

To run an exercise, use
```
node exercise-X.js
```

## Run test 

To run tests, in project root use
```
npm test
```










